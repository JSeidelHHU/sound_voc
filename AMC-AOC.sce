active_buttons = 3;
button_codes = 244, 245, 250;
write_codes = true;

begin;
wavefile { filename="SIN680HZ50MS.wav"; } sinus; 
picture { bitmap { filename="10-AOC.jpg";   }; x = 0; y = 0; } instruction_AOC;
picture { bitmap { filename="16-PAUSE.jpg"; }; x = 0; y = 0; } pause;

array {
	picture { bitmap { filename="08-AMC-R.jpg"; }; x = 0; y = 0; } R;
	picture { bitmap { filename="09-AMC-L.jpg"; }; x = 0; y = 0; } L;
} instruction_AMC;

picture {	
	text { caption = "+";
	font_size = 40;
	};
	x = 0; y = 0;
} fixation;

picture {	
	text { caption = "ext";
	font_size = 40;
	};
	x = 0; y = 0;
} ext;

array {
	trial { #Rechts
		trial_duration = forever;
		trial_type = specific_response;
		terminator_button = 3;
		picture R;
	};
	trial { #Links
		trial_duration = forever;
		trial_type = specific_response;
		terminator_button = 3;
		picture L;
	};
} InstruktionAMC;

trial {
	trial_duration = forever;
	trial_type = specific_response;
	terminator_button = 3;
	picture instruction_AOC;
} InstruktionAOC;

trial {
	trial_duration = forever;
	trial_type = specific_response;
	terminator_button = 3;
	picture pause;
} Pause;

array { #Rechts
	TEMPLATE "AMC.tem" {
		button	time 		code		portcode;
		1		0			"0"			0;
		1		400			"15"		15;
		1		800			"19"		19;
		1		1200		"23"		23;
		1		1800		"29"		29;
	};
} wait_R;

array { #Links
	TEMPLATE "AMC.tem" {
		button	time 		code		portcode;
		2		0			"0"			0;
		2		400			"14"		14;
		2		800			"18"		18;
		2		1200		"22"		22;
		2		1800		"28"		28;
	};
} wait_L;

trial {
	sound { wavefile sinus; };
	picture fixation;
} beep;


array {
	TEMPLATE "AOC.tem" {
		time		code;
		0			246;
		400			115;
		800			119;
		1200		123;
		1600		129;
	};
} playback_R;

array {
	TEMPLATE "AOC.tem" {
		time		code;
		0			247;
		400			114;
		800			118;
		1200		122;
		1600		128;
	};
} playback_L;


begin_pcl;

int nr = 150;


sub string getFilename (string postfix) begin
	string subject = logfile.subject();
	if (subject == "") then
		subject = "test";
	end;
	string format = logfile_directory;
	format.append(subject);
	format.append(postfix);
	string file_name = printf(1, format);
	loop int i = 2; until !file_exists(file_name) begin
		file_name = printf(i, format);
		i = i + 1;
	end;
	return file_name;
end;


sub Save (array <int> recording [nr], array <int> AC [nr], array <int> delay [5], array <bool> cancelled [nr]) begin
	
	output_file file = new output_file;
	file.open_append(getFilename ("_ac_%02d.txt") );
	file.print("\n");
	loop int i = 1 until i > recording.count() begin
		file.print(recording[i]);file.print("\t");file.print(delay[AC[i]]);file.print("\t");file.print(cancelled[i]);file.print("\n");
		i = i + 1;
	end;
	file.close();
end;

sub setTime(trial t, int time) begin
	t.set_duration(time + 50);
	loop int i = 1; until i > t.stimulus_event_count() begin
		stimulus_event e = t.get_stimulus_event(i);
		if (e.event_code() == "110") || (e.event_code() == "111") then
			e.set_time(time);
			return;
		end;
		i = i + 1;
	end;
	term.print_line("not found");
	return;
end;

array <int> hands [2] = { 1, 2 };  # 1=R  2=L 
hands.shuffle();

array <int> delay [5] = {0, 400, 800, 1200, 1800};

	array <int> AC [nr];
	AC.fill(0*nr/10+1,  1*nr/10, 2, 0);
	AC.fill(1*nr/10+1,  2*nr/10, 3, 0);
	AC.fill(2*nr/10+1,  3*nr/10, 4, 0);
	AC.fill(3*nr/10+1,  4*nr/10, 5, 0);
	AC.fill(4*nr/10+1,       nr, 1, 0);
	AC.shuffle();

	array <int> recording [nr];
	array <bool> cancelled [nr];

	InstruktionAMC[hands[1]].present();
	int hand = hands[1];
	loop int i = 1; until i > AC.count()begin
		int onset = clock.time();
		
		if (hand == 1) then
			wait_R[AC[i]].present();
		else
			wait_L[AC[i]].present();
		end;
		beep.present();
		response_data data = response_manager.last_response_data();
		int resp = data.time() - onset;
		
		recording[i] = resp;
		cancelled[i] = delay[AC[i]] > resp;
		
		if (i == nr / 2) then
			InstruktionAMC[hands[2]].present();
			hand = hands[2];
		end;
		i = i + 1;
	end;

	Save(recording, AC, delay, cancelled);

	InstruktionAOC.present();
	hand = hands[1];
	loop int i = 1; until i > recording.count() begin
		trial t1, t2;
		int time = recording[i];
		int rest = time - delay[AC[i]];
		if (hand == 1) then 
			t1 = playback_R[1]; 
			t2 = playback_R[AC[i]]; 
		else 
			t1 = playback_L[1];
			t2 = playback_L[AC[i]];
		end;
		
		if (AC[i] > 1) && (rest > 0) then
			#term.print("# AC:\t");term.print_line(delay[AC[i]]);
			t2.present();
			setTime(t1, rest);
			#term.print("# Rest:\t");term.print_line(rest);
			t1.present();
		else
			#term.print("# Time:\t");term.print_line(time);
			setTime(t1, time);
			t1.present();
		end;
		if (i == nr / 2) then
			Pause.present();
			hand = hands[2];
		end;
		i = i + 1;
	end;




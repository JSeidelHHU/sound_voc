active_buttons = 5;
button_codes = 150, 151, 250, 160, 161;
write_codes = true;

begin;

$delay = 300;

TEMPLATE "instructions.tem";
TEMPLATE "general.tem";
TEMPLATE "AMC.tem";
TEMPLATE "AOC.tem";
TEMPLATE "AVC.tem";
TEMPLATE "VOC.tem";
TEMPLATE "MOC.tem";

begin_pcl;
int delay = 300;
int nr = 61;
#int nr = 10;

array <int> amcHands[2][2] = {{ 1, 2 }, { 2, 1 }};  # 1=R  2=L 
#amcHands[1].shuffle();
#amcHands[2].shuffle();
array <int> mocHands[2][2] = {{ 1, 2 }, { 2, 1 }};  # 1=R  2=L 
#mocHands[1].shuffle();
#mocHands[2].shuffle();

array <int> avcHands[2][2] = {{ 1, 2 }, { 2, 1 }};  # 1=R  2=L 
#avcHands[1].shuffle();
#avcHands[2].shuffle();
array <int> vocHands[2][2] = {{ 1, 2 }, { 2, 1 }};  # 1=R  2=L 
#vocHands[1].shuffle();
#vocHands[2].shuffle();

int proband1 = 1;
int proband2 = 2;
int hand1 = 1;
int hand2 = 2;

array <int> amcrecording [nr][2];
array <int> mocrecording [nr][2];

include "instructions.pcl";
include "Recording.pcl";
include "general.pcl";
include "AMC.pcl";
include "MOC.pcl";
include "AOC.pcl";

include "AVC.pcl";
include "VOC.pcl";

#loop  int i=1; until i> 60 begin amcrecording[i][1] = 1000; amcrecording[i][2] = random(100, 400);  i = i + 1; end;
#loop  int i=1; until i> 60 begin mocrecording[i][1] = 1000; mocrecording[i][2] = random(100, 400);  i = i + 1; end;

amcrecording = RunAmc(proband1, amcHands[proband1][hand1]);
mocrecording = RunMoc(proband2, mocHands[proband2][hand1]);
RunAoc(                         amcHands[proband1][hand1], amcrecording);

mocrecording = RunMoc(proband1, mocHands[proband1][hand1]);
amcrecording = RunAmc(proband2, amcHands[proband2][hand1]);
RunVoc(                         mocHands[proband1][hand1], mocrecording);
RunAvc(			                 amcHands[proband2][hand1], amcrecording);

amcrecording = RunAmc(proband1, amcHands[proband1][hand2]);
mocrecording = RunMoc(proband2, mocHands[proband2][hand2]);
RunAoc(                         amcHands[proband1][hand2], amcrecording);

mocrecording = RunMoc(proband1, mocHands[proband1][hand2]);
amcrecording = RunAmc(proband2, amcHands[proband2][hand2]);
RunVoc(                         mocHands[proband1][hand2], mocrecording);
RunAvc(                         amcHands[proband2][hand2], amcrecording);

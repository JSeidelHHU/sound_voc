active_buttons = 3;
button_codes = 150, 151, 250;
write_codes = true;

begin;
TEMPLATE "instructions.tem";
TEMPLATE "general.tem";

array{
	trial {
		trial_type = fixed;
		trial_duration = 2400;
		sound { wavefile sinus; };
		time=1200;
		port_code=140;
		code="140";
	};
	trial {
		trial_type = fixed;
		trial_duration = 2400;
		sound { wavefile sinus; };
		time=1200;
		port_code=141;
		code="141";
	};
	
}metbeep;

begin_pcl;
include "general.pcl";
include "instructions.pcl";

include "hands.pcl";

#Instructions[instrEye1].present();
#Instructions[instrEye2].present();
Instructions[instrGeneral].present();
ClearScreen.present();

loop int proband = 1 until proband > 2 begin
	Blockmarker(proband);
	loop int h = 1 until h > hands.count() begin
		hand = hands[h];
		if h == 1 then 
			int pic = 3 + ((proband - 1) * 2) + hand;
			Instructions[pic].present();
		end;
		int hits = 0;

		loop int i = 1; until i > 30 begin
			int onset = clock.time();
			metbeep[hand].present();
			response_data data = response_manager.last_response_data();
			int resp = data.time();
			int ttime = resp - onset;
			if (ttime >= 600 && ttime <= 1800 ) then
				hits = hits + 1;
			end;
			i = i + 1;
		end;
		h = h + 1;
		if h < 3 then
			if proband == 1 then
				Instructions[10 + hands[h]].present();
			end;
			if proband == 2 then
				Instructions[14 + hands[h]].present();
			end;
		end;
	end;
	proband = proband + 1;
end;
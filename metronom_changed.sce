active_buttons = 3;
button_codes = 112, 113, 100;

begin;
TEMPLATE "instructions.tem";
TEMPLATE "general.tem";

trial {
	trial_type = fixed;
	trial_duration = 2400;
	sound { wavefile sinus; };
	time=1200;
}metbeep;


begin_pcl;
include "general.pcl"; # manu was here
include "instructions.pcl";

array <int> hands [2] = { 1, 2 }; 
hands.shuffle();

Instructions[instrEye1].present();
Instructions[instrEye2].present();
Instructions[instrGeneral].present();

loop int proband = 1 until proband > 2 begin
	Blockmarker(proband);
	loop int h = 1 until h > hands.count() begin
		int hand = hands[h];
		int pic = 3 + ((proband - 1) * 2) + hand;
		Instructions[pic].present();
		int hits = 0;

		loop int i = 1; until i > 30 begin
			int onset = clock.time();
			metbeep.present(); # metbeep[hand].present();  --> old
			response_data data = response_manager.last_response_data();
			int resp = data.time();
			int ttime = resp - onset;
			if (ttime >= 600 && ttime <= 1800 ) then
				hits = hits + 1;
			end;
			i = i + 1;
		end;
		
		h = h + 1;
	end;
	proband = proband + 1;
end;
active_buttons = 3;
button_codes = 150, 151, 250;
write_codes = true;

begin;
TEMPLATE "instructions.tem";
TEMPLATE "general.tem";


 picture { bitmap { filename="Instructions/Folie18.jpg"; }; x = 0; y = 0; } fast;
 picture { bitmap { filename="Instructions/Folie17.jpg"; }; x = 0; y = 0; } slow;

$delay = 300;

$fastR = 142;
$fastL = 143;
$corrR = 144;
$corrL = 145;
$slowR = 146;
$slowL = 147;

array {
	trial{
		stimulus_event {
			sound { wavefile sinus; };
			time = $delay;
			code="$corrR";
			port_code=$corrR;
		};
	}correct_R;
	trial{
		stimulus_event {
			sound { wavefile sinus; };
			time = $delay;
			code="$corrL";
			port_code=$corrL;
		};
	}correct_L;
} correct;

array {
	trial{
		trial_duration = 500;
		#stimulus_event {
			sound { wavefile sinus; };
			time = $delay;
		#}
		#stimulus_event {
			picture fast;
			code="$fastR";
			port_code=$fastR;
		#};
	};
	trial{
		trial_duration = 500;
		#stimulus_event {
			sound { wavefile sinus; };
			time = $delay;
		#};
		#stimulus_event {
			picture fast;
			code="$fastL";
			port_code=$fastL;
		#};
	};
} tooFast;

array {
	trial{
		trial_duration = 500;
		#stimulus_event {
			sound { wavefile sinus; };
			time = $delay;
		#};
		#stimulus_event {
			picture slow;
			code="$slowR";
			port_code=$slowR;
		#};
	};
	trial{
		trial_duration = 500;
		#stimulus_event {
			sound { wavefile sinus; };
			time = $delay;
		#};
		#stimulus_event {
			picture slow;
			code="$slowL";
			port_code=$slowL;
		#};
	};
} tooSlow;


begin_pcl;
include "instructions.pcl";
include "general.pcl";


sub Accuracy (int proband, int hits, int total) begin
	string subject = logfile.subject();
	if (subject == "") then
		subject = "test";
	end;
	string file_name = "Accuracy.txt";
	output_file file = new output_file;
	file.open_append("Accuracy.txt");

	file.print(subject);file.print("\t");file.print(proband);file.print("\t");file.print(hits * 100 / total);file.print("%\n");
	term.print("training accuracy: "); term.print(hits * 100 / total); term.print_line("%");
	file.close();

end;

include "hands.pcl";

loop int proband = 1 until proband > 2 begin
   Blockmarker(2 + proband);
	int hits = 0;
	loop int h = 1 until h > hands.count() begin
		hand = hands[h];
		ClearScreen.present();
		int pic = 8 + ((proband - 1) * 4) + ((h - 1) * 2) + hand;
		Instructions[pic].present();
		Reminder.present();
		loop
			int i = 1;
			int onset = clock.time();
		until i > 60 begin
			
			wait[hand].present();
			response_data data = response_manager.last_response_data();
			int resp = data.time();
			int ttime = resp - onset;
			if (ttime < 1800) then
				#fast
				tooFast[hand].present(); 
				#Ref[hand].present();
			else 
				if (ttime > 3000) then
					#slow
					tooSlow[hand].present();
					#Ref[hand].present();
				else
					#in time
					correct[hand].present();
					hits = hits + 1;
				end;
			end;
			onset = resp;
			i = i + 1;
		end;
		h = h + 1;
	end;
	Accuracy(proband, hits, 120);
	proband = proband + 1;
end;

delete_hand();

